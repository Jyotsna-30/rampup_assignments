package com.hcl.pharmacymanagementsystem.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hcl.pharmacymanagementsystem.Service.PharmasistService;
import com.hcl.pharmacymanagementsystem.model.Pharmasist;

@Controller
@RequestMapping("/admin")
public class AdminController {
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminController.class);
	@Autowired
	PharmasistService pharmasistService;

	@PutMapping("/registerPharmasist")
	public ResponseEntity<Pharmasist> savePharmasist(@RequestBody Pharmasist pharmasist) {
		try {
			Pharmasist _pharmasist = pharmasistService.savePharmasist(pharmasist);
			return ResponseEntity.status(HttpStatus.OK).body(_pharmasist);
		} catch (Exception ex) {
			LOGGER.error("Failed to save the pharmasist");
			return null;
		}
	}

	@GetMapping("/getAllPharmasists")
	public ResponseEntity<List<Pharmasist>> getAllPharmasists() {
		try {
			List<Pharmasist> pharmasists = pharmasistService.getAllPharmasists();
			if (pharmasists == null)
				return new ResponseEntity<List<Pharmasist>>(HttpStatus.NO_CONTENT);
			else {
				LOGGER.info("Pharmasists List Obtained Successfully");
				return ResponseEntity.status(HttpStatus.OK).body(pharmasists);
			}
		} catch (Exception ex) {
			LOGGER.error("Pharmasists list is empty");
			return null;
		}

	}

}
