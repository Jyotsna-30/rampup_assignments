package com.hcl.pharmacymanagementsystem.controller;

import java.util.List;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hcl.pharmacymanagementsystem.Service.ProductService;
import com.hcl.pharmacymanagementsystem.model.Product;

@Controller
@RequestMapping("/product")
public class ProductController {
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

	@Autowired
	ProductService productService;

	@PostMapping("/save")
	public ResponseEntity<Product> saveProduct(@RequestBody Product _product){
		try {
		Product product = productService.saveProduct(_product);
		LOGGER.info("saved successfully" + product.getProduct_name());
		HttpHeaders header = new HttpHeaders();
		header.add("save", "Saving the product");
		return ResponseEntity.status(HttpStatus.OK).headers(header).body(product);
		}
		catch(Exception ex)
		{
			LOGGER.error("Error in saving the product");
			return null;
		}
	}

	@GetMapping("/getProduct/{id}")
	public ResponseEntity<Product> getProductById(@PathVariable int id) {
		try {
		Product product = productService.getProductById(id);
		if (product == null) {		
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(product);
		} else {
			LOGGER.info("Product details are obtained successfully");
			return ResponseEntity.status(HttpStatus.FOUND).body(product);
		}
		}
		catch(NoSuchElementException ex)
		{
			LOGGER.error("Product with the mentioned id is not present");
			return null;
		}
	}

	@GetMapping("/getAllProducts")
	public ResponseEntity<List<Product>> getAllProducts() {
		try {
		List<Product> products = productService.getAllProducts();
		if (products == null)
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(products);
		else {
			
				LOGGER.info("Products list is obtained successfully");
			return ResponseEntity.status(HttpStatus.OK).body(products);
		}
	}
		catch(Exception ex)
		{
			LOGGER.error("Error in fetching the products list");
			return null;
		}
	}
	

	@PutMapping("/updateProduct")
	public ResponseEntity<Product> updateProduct(@RequestBody Product _product) {
		try {
		Product product = productService.getProductById(_product.getProduct_id());
		if (product == null)
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(product);
		else {
			productService.updateProduct(_product);
			return ResponseEntity.status(HttpStatus.OK).body(product);
		}
		}
		catch(NoSuchElementException ex)
		{
			LOGGER.error("Product with the mentioned id is not present");
			return null;
		}
	}

	@PutMapping("/updateProduct/{id}")
	public ResponseEntity<Product> updateProductById(@PathVariable int id) {
		try {
		Product product = productService.getProductById(id);
		if (product == null)
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(product);
		else {
			productService.updateProduct(product);
			LOGGER.info(product.getProduct_name()+ "updated successfully");
			return ResponseEntity.status(HttpStatus.OK).body(product);
		}
		}
		catch(NoSuchElementException ex)
		{
			LOGGER.error("Error in updating the Product");
			return null;
		}
	}

	@DeleteMapping("/deleteById/{id}")
	public ResponseEntity<Product> deleteProductById(@PathVariable int id) {
		try {
		Product product = productService.getProductById(id);
		if (product == null)
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(product);
		else {
			productService.deleteProductById(id);
			LOGGER.info(product.getProduct_name()+"deleted successfully");
			return new ResponseEntity<Product>(HttpStatus.OK);
		}
	}
		catch(NoSuchElementException ex)
		{
			LOGGER.error("Error in deleting the product");
			return null;
		}

}
}
