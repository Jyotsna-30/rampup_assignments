package com.hcl.pharmacymanagementsystem.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Pharmasist {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int pharmasist_id;
	private String pharmasist_name;
	private long phoneNumber;
	@Column(unique = true)
	private String email;
	private String password;
	public int getPharmasist_id() {
		return pharmasist_id;
	}
	public void setPharmasist_id(int pharmasist_id) {
		this.pharmasist_id = pharmasist_id;
	}
	public String getPharmasist_name() {
		return pharmasist_name;
	}
	public void setPharmasist_name(String pharmasist_name) {
		this.pharmasist_name = pharmasist_name;
	}
	public long getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Pharmasist(int pharmasist_id, String pharmasist_name, long phoneNumber, String email, String password) {
		super();
		this.pharmasist_id = pharmasist_id;
		this.pharmasist_name = pharmasist_name;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.password = password;
	}
	public Pharmasist() {
		super();
	}
	@Override
	public String toString() {
		return "Pharmasist [pharmasist_id=" + pharmasist_id + ", pharmasist_name=" + pharmasist_name + ", phoneNumber="
				+ phoneNumber + ", email=" + email + ", password=" + password + "]";
	}
	
	
}
