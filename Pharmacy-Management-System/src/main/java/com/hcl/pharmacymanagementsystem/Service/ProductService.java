package com.hcl.pharmacymanagementsystem.Service;

import java.util.List;

import com.hcl.pharmacymanagementsystem.model.Product;

public interface ProductService {

	public Product saveProduct(Product product);
	public Product updateProduct(Product product);
	public Product getProductById(int product_id);
	public int deleteProductById(int product_id);
	public List<Product> getAllProducts();
	public Product updateProduct(int product_id);

}
