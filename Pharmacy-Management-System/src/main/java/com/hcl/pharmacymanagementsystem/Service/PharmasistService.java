package com.hcl.pharmacymanagementsystem.Service;

import java.util.List;

import com.hcl.pharmacymanagementsystem.model.Pharmasist;

public interface PharmasistService {
	public Pharmasist savePharmasist(Pharmasist pharmasist);
	public List<Pharmasist> getAllPharmasists();

}
