package com.hcl.pharmacymanagementsystem.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.hcl.pharmacymanagementsystem.dao.ProductRepository;
import com.hcl.pharmacymanagementsystem.model.Product;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductRepository productRepository;

	@Override
	public Product saveProduct(Product product) {
		productRepository.save(product);
		return product;
	}

	@Override
	public Product updateProduct(int product_id) {
		Product product=productRepository.findById(product_id).get();
		Product updatedProduct=productRepository.save(product);
		return updatedProduct;

	}

	@Override
	public Product getProductById(int product_id) {
		Product product = productRepository.findById(product_id).get();
		return product;
	}

	@Override
	public int deleteProductById(int product_id) {
		productRepository.deleteById(product_id);
		return product_id;

	}

	@Override
	public List<Product> getAllProducts() {
	    List<Product> products=  productRepository.findAll();
	    return products;
	}

	@Override
	public Product updateProduct(Product product) {
		Product updatedProduct=productRepository.save(product);
		return updatedProduct;
	}

}
