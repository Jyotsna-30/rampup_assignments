package com.hcl.pharmacymanagementsystem.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.pharmacymanagementsystem.dao.PharmasistRepository;
import com.hcl.pharmacymanagementsystem.model.Pharmasist;

@Service
public class PharmasistServiceImpl implements PharmasistService{

	@Autowired
	PharmasistRepository pharmasistRepository;
	@Override
	public Pharmasist savePharmasist(Pharmasist pharmasist) {
		pharmasistRepository.save(pharmasist);
		return pharmasist;
	}
	@Override
	public List<Pharmasist> getAllPharmasists() {
		List<Pharmasist> pharmasists=pharmasistRepository.findAll();
		return pharmasists;
	}
	

}
