package com.hcl.pharmacymanagementsystem.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.pharmacymanagementsystem.model.Pharmasist;

public interface PharmasistRepository extends JpaRepository<Pharmasist,Integer> {

}
