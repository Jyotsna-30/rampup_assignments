package com.hcl.pharmacymanagementsystem.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.pharmacymanagementsystem.model.Admin;

public interface AdminRepository extends JpaRepository<Admin,Integer>{

}
