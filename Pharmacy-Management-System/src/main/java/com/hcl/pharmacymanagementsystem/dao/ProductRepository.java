package com.hcl.pharmacymanagementsystem.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.hcl.pharmacymanagementsystem.model.Product;
@Repository
public interface ProductRepository extends PagingAndSortingRepository<Product,Integer> ,JpaRepository<Product,Integer> {

}
