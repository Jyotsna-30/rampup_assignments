package com.hcl.carbookingsystem.model;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Test {
	public static void main(String[] args) {
		
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");
		SessionFactory factory = cfg.buildSessionFactory();
		Vehicle vehicle1 = new Vehicle(101, "Car", "4 Seater", "AP1001");
		Vehicle vechile2 = new Vehicle(102, "Bus", "20 Seater", "KA2233");
		List<Vehicle> vehicles = new ArrayList<Vehicle>();
		vehicles.add(vehicle1);
		vehicles.add(vechile2);
		User user1 = new User(1, "jyo", "BNj123G", vehicles);
		//User user2 = new User(2, "Remi", "JU457G", vehicles);
		List<User> users=new ArrayList<>();
		users.add(user1);
		Driver driver=new Driver(301,"Raju","A1234f");
		Booking booking = new Booking(user1,vehicle1,driver);
		//Payment payment=new Payment(booking,"Chennai","Banglore",1000.00f,"Card","Success");
	//	System.out.println(payment.getBooking().getDriver());
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		/*session.save(user1);
		session.save(vechile1);
		session.save(vechile2);
		session.save(user2);*/
		session.save(booking);
		//session.save(payment);
		tx.commit();
	}
}
