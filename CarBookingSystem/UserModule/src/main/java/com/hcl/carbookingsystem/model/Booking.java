package com.hcl.carbookingsystem.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Booking {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int booking_id;
	@OneToOne(targetEntity=User.class)
	private User user;
	@OneToOne(targetEntity=Vehicle.class)
	private Vehicle vehicle;
	@OneToOne(targetEntity=Driver.class)
	private Driver driver;
	public int getBooking_id() {
		return booking_id;
	}
	public void setBooking_id(int booking_id) {
		this.booking_id = booking_id;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Vehicle getVehicle() {
		return vehicle;
	}
	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
	public Driver getDriver() {
		return driver;
	}
	public void setDriver(Driver driver) {
		this.driver = driver;
	}
	public Booking( User user, Vehicle vehicle, Driver driver) {
		super();
		this.user = user;
		this.vehicle = vehicle;
		this.driver = driver;
	}
	public Booking() {
		super();
	}
	
	public Booking(int booking_id, User user, Vehicle vehicle, Driver driver) {
		super();
		this.booking_id = booking_id;
		this.user = user;
		this.vehicle = vehicle;
		this.driver = driver;
	}
	@Override
	public String toString() {
		return "Booking [booking_id=" + booking_id + ", user=" + user + ", vehicle=" + vehicle + ", driver=" + driver
				+ "]";
	}
	
}
