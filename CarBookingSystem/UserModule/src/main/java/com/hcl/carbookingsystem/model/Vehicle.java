package com.hcl.carbookingsystem.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;


@Entity
public class Vehicle {
	@Id
	private int vehicle_id;
	@Column(nullable=false)
	private String vehicle_name;
	@Column(nullable=false)
	private String vehicle_type;
	private String vehicle_RegNo;
	@ManyToMany(mappedBy = "vehicles")
	private List<User> users;
	
	public int getVehicle_id() {
		return vehicle_id;
	}

	public void setVehicle_id(int vehicle_id) {
		this.vehicle_id = vehicle_id;
	}

	public String getVehicle_name() {
		return vehicle_name;
	}

	public void setVehicle_name(String vehicle_name) {
		this.vehicle_name = vehicle_name;
	}

	public String getVehicle_type() {
		return vehicle_type;
	}

	public void setVehicle_type(String vehicle_type) {
		this.vehicle_type = vehicle_type;
	}

	public String getVehicle_RegNo() {
		return vehicle_RegNo;
	}

	public void setVehicle_RegNo(String vehicle_RegNo) {
		this.vehicle_RegNo = vehicle_RegNo;
	}

	

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public Vehicle() {
		super();
	}
	

	public Vehicle(int vehicle_id, String vehicle_name, String vehicle_type, String vehicle_RegNo, List<User> users) {
		super();
		this.vehicle_id = vehicle_id;
		this.vehicle_name = vehicle_name;
		this.vehicle_type = vehicle_type;
		this.vehicle_RegNo = vehicle_RegNo;
		this.users = users;
	}

	public Vehicle(int vehicle_id, String vehicle_name, String vehicle_type, String vehicle_RegNo) {
		super();
		this.vehicle_id = vehicle_id;
		this.vehicle_name = vehicle_name;
		this.vehicle_type = vehicle_type;
		this.vehicle_RegNo = vehicle_RegNo;
	}

	@Override
	public String toString() {
		return "Vehicle [vehicle_id=" + vehicle_id + ", vehicle_name=" + vehicle_name + ", vehicle_type=" + vehicle_type
				+ ", vehicle_RegNo=" + vehicle_RegNo + ", users=" + users + "]";
	}

}
