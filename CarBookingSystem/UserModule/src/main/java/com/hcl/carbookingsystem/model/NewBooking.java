package com.hcl.carbookingsystem.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class NewBooking {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int Booking_id;
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="user_Id")
	private User user;
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="vehicle_id")
	private Vehicle vehicle;
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="driver_id")
	private Driver driver;
	private String Source;
	private String Destination;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public Driver getDriver() {
		return driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	public NewBooking(User user, Vehicle vehicle, Driver driver) {
		super();
		this.user = user;
		this.vehicle = vehicle;
		this.driver = driver;
	}

	public NewBooking() {
		super();
	}

	@Override
	public String toString() {
		return "NewBooking [user=" + user + ", vehicle=" + vehicle + ", driver=" + driver + "]";
	}

	
	public NewBooking(User user, Vehicle vehicle, Driver driver, String source, String destination) {
		super();
		this.user = user;
		this.vehicle = vehicle;
		this.driver = driver;
		Source = source;
		Destination = destination;
	}

	public String getSource() {
		return Source;
	}

	public void setSource(String source) {
		Source = source;
	}

	public String getDestination() {
		return Destination;
	}

	public void setDestination(String destination) {
		Destination = destination;
	}

}
