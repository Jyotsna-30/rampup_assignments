package com.hcl.carbookingsystem.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Driver {
	@Id
	private int driver_id;
	@Column(nullable = false)
	private String driver_name;
	private String driver_licenceNo;
	private String availability;

	public int getDriver_id() {
		return driver_id;
	}

	public void setDriver_id(int driver_id) {
		this.driver_id = driver_id;
	}

	public String getDriver_name() {
		return driver_name;
	}

	public void setDriver_name(String driver_name) {
		this.driver_name = driver_name;
	}

	public String getDriver_licenceNo() {
		return driver_licenceNo;
	}

	public void setDriver_licenceNo(String driver_licenceNo) {
		this.driver_licenceNo = driver_licenceNo;
	}

	public String getAvailability() {
		return availability;
	}

	public void setAvailability(String availability) {
		this.availability = availability;
	}

	public Driver() {
		super();
	}

	public Driver(int driver_id, String driver_name, String driver_licenceNo) {
		super();
		this.driver_id = driver_id;
		this.driver_name = driver_name;
		this.driver_licenceNo = driver_licenceNo;
	}

	public Driver(int driver_id, String driver_name, String driver_licenceNo, String availability) {
		super();
		this.driver_id = driver_id;
		this.driver_name = driver_name;
		this.driver_licenceNo = driver_licenceNo;
		this.availability = availability;
	}

	@Override
	public String toString() {
		return "Driver [driver_id=" + driver_id + ", driver_name=" + driver_name + ", driver_licenceNo="
				+ driver_licenceNo + "]";
	}

}
