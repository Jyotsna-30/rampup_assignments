package com.hcl.carbookingsystem.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Payment {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int payment_id;
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="booking_id")
	private NewBooking booking;
	private float amount;
	private String payment_method;
	private String status;

	public int getPayment_id() {
		return payment_id;
	}
	public void setPayment_id(int payment_id) {
		this.payment_id = payment_id;
	}
	
	public NewBooking getBooking() {
		return booking;
	}
	public void setBooking(NewBooking booking) {
		this.booking = booking;
	}
	public float getAmount() {
		return amount;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
	public String getPayment_method() {
		return payment_method;
	}
	public void setPayment_method(String payment_method) {
		this.payment_method = payment_method;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public Payment() {
		super();
	}
	
	public Payment( NewBooking booking, float amount,
			String payment_method, String status) {
		super();
		this.booking = booking;
		this.amount = amount;
		this.payment_method = payment_method;
		this.status = status;
	}

	@Override
	public String toString() {
		return "Payment [payment_id=" + payment_id + ", booking=" + booking + ", amount=" + amount + ", payment_method="
				+ payment_method + ", status=" + status + "]";
	}
	
     
}
