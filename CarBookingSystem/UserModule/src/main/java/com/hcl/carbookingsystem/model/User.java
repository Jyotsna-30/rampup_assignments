package com.hcl.carbookingsystem.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class User {
	@Id
	private int user_id;
	@Column(nullable = false)
	private String user_name;
	private String user_PANcard;
	@ManyToMany
	@JoinTable(name = "user_Vehicle", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = {
			@JoinColumn(name = "vehicle_id") })
	private List<Vehicle> vehicles;

	public List<Vehicle> getVehicles() {
		return vehicles;
	}

	public void setVehicles(List<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getUser_PANcard() {
		return user_PANcard;
	}

	public void setUser_PANcard(String user_PANcard) {
		this.user_PANcard = user_PANcard;
	}

	public User(int user_id, String user_name, String user_PANcard) {
		super();
		this.user_id = user_id;
		this.user_name = user_name;
		this.user_PANcard = user_PANcard;
	}

	public User() {
		super();
	}

	public User(int user_id, String user_name, String user_PANcard, List<Vehicle> vehicles) {
		super();
		this.user_id = user_id;
		this.user_name = user_name;
		this.user_PANcard = user_PANcard;
		this.vehicles = vehicles;
	}

	@Override
	public String toString() {
		return "User [user_id=" + user_id + ", user_name=" + user_name + ", user_PANcard=" + user_PANcard + "]";
	}

}
