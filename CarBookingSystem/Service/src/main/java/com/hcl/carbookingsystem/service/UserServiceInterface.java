package com.hcl.carbookingsystem.service;

import java.util.List;

import com.hcl.carbookingsystem.model.User;

public interface UserServiceInterface {
	public User saveUser(User user);

	public User updateUser(User user);

	public User getUser(int user_id);
	
	public List<User> getAllUsers();

	public void deleteUser(User user);

}
