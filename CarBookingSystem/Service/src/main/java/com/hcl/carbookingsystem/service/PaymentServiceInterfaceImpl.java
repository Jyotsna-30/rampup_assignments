package com.hcl.carbookingsystem.service;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hcl.carbookingsystem.model.Payment;
import static com.hcl.carbookingsystem.utility.Utility.getSession;;

public class PaymentServiceInterfaceImpl implements PaymentServiceInterface{

	public static Session session=getSession();
	public Transaction transaction;
	@Override
	public Payment makePayment(Payment payment) {
		transaction=session.beginTransaction();
		session.persist(payment);
		transaction.commit();
		return payment;
	}

	@Override
	public Payment getPaymentDetails(int payment_id) {
		Payment payment=session.get(Payment.class,payment_id);
		return payment;
		
	}

	@Override
	public Payment updatePayment(Payment payment) {
		transaction=session.beginTransaction();
		session.update(payment);
		transaction.commit();
		return payment;
	}

	@Override
	public void canclePayment(Payment payment) {
		transaction=session.beginTransaction();
		session.delete(payment);
		transaction.commit();
	}	

}
