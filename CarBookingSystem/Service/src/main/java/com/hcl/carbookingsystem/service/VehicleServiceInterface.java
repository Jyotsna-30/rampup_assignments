package com.hcl.carbookingsystem.service;

import java.util.List;

import com.hcl.carbookingsystem.model.Vehicle;

public interface VehicleServiceInterface {
	public Vehicle saveVehicle(Vehicle vehicle);

	public Vehicle updateVehicle(Vehicle vehicle);

	public Vehicle getVehicle(int vehicle_id);
	
	public List<Vehicle> getAllVehicles();

	public void deleteVehicle(Vehicle vehicle);

}
