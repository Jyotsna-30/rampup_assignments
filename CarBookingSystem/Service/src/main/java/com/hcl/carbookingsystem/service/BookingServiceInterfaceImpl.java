package com.hcl.carbookingsystem.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hcl.carbookingsystem.model.NewBooking;
import com.hcl.carbookingsystem.model.User;
import com.hcl.carbookingsystem.utility.Utility;

public class BookingServiceInterfaceImpl implements BookingServiceInterface {
 
	static Session session=Utility.getSession();
	public Transaction transaction;
	@Override
	public NewBooking book_A_Car(NewBooking booking){
		transaction=session.beginTransaction();
		session.save(booking);
		transaction.commit();
		return booking;
	}

	@Override
	public NewBooking updateBooking(NewBooking booking) {
		transaction=session.beginTransaction();
		session.save(booking);
		transaction.commit();
		return booking;
	}

	@Override
	public NewBooking getBookingDetails(int booking_id) {
		NewBooking booking=session.get(NewBooking.class,booking_id);
		return booking;
	}

	@Override
	public void cancelBooking(NewBooking booking) {
	transaction=session.beginTransaction();
	session.delete(booking);
    transaction.commit();		
	}

	@Override
	public List<NewBooking> getAllBookings() {
		List<NewBooking> bookings=session.createQuery("from newbooking",NewBooking.class).list();
		return bookings;
	}

	@Override
	public List<NewBooking> getAllBookingsofUser(User user) {
		//int id=user.getUser_id();
		List<NewBooking> bookings=session.createNativeQuery("select * from newbooking where user_id=:id",NewBooking.class).setParameter("id",user.getUser_id()).list();
		
		return bookings;
	}

}
