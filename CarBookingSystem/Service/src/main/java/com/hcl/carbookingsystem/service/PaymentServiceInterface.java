package com.hcl.carbookingsystem.service;

import com.hcl.carbookingsystem.model.Payment;

public interface PaymentServiceInterface {
	public Payment makePayment(Payment payment);
	public Payment getPaymentDetails(int payment_id);
	public Payment updatePayment(Payment payment);
	public void canclePayment(Payment payment);

}
