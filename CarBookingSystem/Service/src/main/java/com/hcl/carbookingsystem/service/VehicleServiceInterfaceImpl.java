package com.hcl.carbookingsystem.service;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hcl.carbookingsystem.model.Vehicle;
import com.hcl.carbookingsystem.utility.Utility;

public class VehicleServiceInterfaceImpl implements VehicleServiceInterface {
	static Session session = Utility.getSession();
	Transaction transaction;

	@Override
	public Vehicle saveVehicle(Vehicle vehicle) {
		transaction=session.beginTransaction();
		session.persist(vehicle);
		transaction.commit();
		return vehicle;
	}

	@Override
	public Vehicle updateVehicle(Vehicle vehicle) {
		transaction=session.beginTransaction();
		try {
		session.update(vehicle);
		transaction.commit();
		}
		catch(Exception ex)
		{
			System.err.println(ex);
		}
		return vehicle;
	}

	@Override
	public Vehicle getVehicle(int vehicle_id) {
		Vehicle vehicle = session.get(Vehicle.class, vehicle_id);
		return vehicle;
	}

	@Override
	public void deleteVehicle(Vehicle vehicle) {
		transaction=session.beginTransaction();
		session.delete(vehicle);
		transaction.commit();
	}

	@Override
	public List<Vehicle> getAllVehicles() {
		List<Vehicle> vehicles=session.createQuery("from Vehicle",Vehicle.class).list();
		return vehicles;
	}

}
