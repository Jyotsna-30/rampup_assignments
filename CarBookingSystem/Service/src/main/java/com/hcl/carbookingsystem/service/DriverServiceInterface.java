package com.hcl.carbookingsystem.service;

import java.util.List;

import com.hcl.carbookingsystem.model.Driver;

public interface DriverServiceInterface {
	public Driver saveDriver(Driver driver);
	public Driver getDriver(int driver_id);
	public List<Driver> getAllDrivers();
	public Driver updateDriver(Driver driver);
	public void deleteDriver(Driver driver);

}
