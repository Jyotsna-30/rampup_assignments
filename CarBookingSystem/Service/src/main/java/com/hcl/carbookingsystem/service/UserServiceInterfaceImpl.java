package com.hcl.carbookingsystem.service;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hcl.carbookingsystem.model.User;
import com.hcl.carbookingsystem.utility.Utility;

public class UserServiceInterfaceImpl implements UserServiceInterface {

	static Session session = Utility.getSession();
	Transaction transaction;

	@Override
	public User saveUser(User user) {
		transaction = session.beginTransaction();
		session.persist(user);
		transaction.commit();
		return user;
	}

	@Override
	public User updateUser(User user) {
		transaction = session.beginTransaction();
		session.update(user);
		transaction.commit();
		return user;
	}

	@Override
	public User getUser(int user_id) {
		
		User user = (User) session.get(User.class, user_id);
		return user;
	}

	@Override
	public void deleteUser(User user) {
		transaction = session.beginTransaction();
		session.delete(user);
		transaction.commit();
	}

	@Override
	public List<User> getAllUsers() {
		
		List<User> users=session.createQuery("from User",User.class).list();
		return users;
	}
}
