package com.hcl.carbookingsystem.service;

import java.util.List;

import com.hcl.carbookingsystem.model.NewBooking;
import com.hcl.carbookingsystem.model.User;

public interface BookingServiceInterface {
	public NewBooking book_A_Car(NewBooking booking);

	public NewBooking updateBooking(NewBooking booking);

	public List<NewBooking> getAllBookings();
	
	public List<NewBooking> getAllBookingsofUser(User user);

	public NewBooking getBookingDetails(int booking_id);

	public void cancelBooking(NewBooking booking);

}
