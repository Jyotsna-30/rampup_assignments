package com.hcl.carbookingsystem.service;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hcl.carbookingsystem.model.Driver;
import com.hcl.carbookingsystem.utility.Utility;

public class DriverServiceInterfaceImpl implements DriverServiceInterface {
	static Session session = Utility.getSession();
	Transaction transaction;

	@Override
	public Driver saveDriver(Driver driver) {
		transaction = session.beginTransaction();
		session.persist(driver);
		transaction.commit();
		return driver;
	}

	@Override
	public Driver getDriver(int driver_id) {
		Driver driver=session.get(Driver.class, driver_id);
		return driver;
	}

	@Override
	public Driver updateDriver(Driver driver) {
		transaction = session.beginTransaction();
		session.update(driver);
		transaction.commit();
		return driver;
	}

	@Override
	public void deleteDriver(Driver driver) {
		transaction = session.beginTransaction();
		session.delete(driver);
		transaction.commit();
	}

	@Override
	public List<Driver> getAllDrivers() {
		List<Driver> drivers=session.createQuery("from Driver",Driver.class).list();
		return drivers;
	}

}
