package com.hcl.carbookingsystem.test;

import java.util.List;

import org.testng.annotations.Test;

import com.hcl.carbookingsystem.model.User;
import com.hcl.carbookingsystem.service.UserServiceInterface;
import com.hcl.carbookingsystem.service.UserServiceInterfaceImpl;

public class UserTestCases {
	UserServiceInterface userService = new UserServiceInterfaceImpl();
	public User user1;
	public User user2;
	public User user3;

	@Test
	public void saveUser() {
		user1 = new User(1, "jo", "BJ133GV");
		user2 = new User(2, "Remi", "K134G8");
		user3 = new User(3, "Amy", "13Ds45");
		userService.saveUser(user1);
		userService.saveUser(user2);
		userService.saveUser(user3);
	}

	@Test(dependsOnMethods = { "saveUser" })
	public void getUser() {
		User user = userService.getUser(1);
		System.out.println(user);
	}

	@Test(dependsOnMethods = { "saveUser" })
	public void updateUser() {
		user1.setUser_name("jyo");
		userService.updateUser(user1);
	}

	@Test
	public void getAllUsers() {
		List<User> users = userService.getAllUsers();
		for (User user : users) {
			System.out.println(user);
		}
	}

	@Test(dependsOnMethods = { "saveUser" })
	public void deleteUser() {
		userService.deleteUser(user3);
	}
}
