package com.miniproject.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.miniproject.model.Course;
import com.miniproject.model.CourseRegistration;
import com.miniproject.model.User;
import com.miniproject.repository.CourseRegistrationRepository;
import com.miniproject.repository.CourseRepository;
import com.miniproject.repository.UserRepository;

@Service
public class CourseServiceImplementation implements CourseService {
	@Autowired
	CourseRepository courseRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	CourseRegistrationRepository courseRegistrationRepository;

	@Override
	public Course saveCourse(Course course) {
		return courseRepository.save(course);
	}

	@Override
	public Course getCourseByCourseName(String courseName) {
		return courseRepository.findCourseByCourseName(courseName);
	}

	@Override
	public List<Course> getAllCourses() {
		return courseRepository.findAll();
	}

	@Override
	public CourseRegistration enrollCourse(CourseRegistration courseRegistration) {
		courseRegistration.setStatus("Enrolled");
		courseRegistration.setCompletionStatus("Enrolled");
		courseRegistration.setRemarks("Waiting for Approval");
		return courseRegistrationRepository.save(courseRegistration);
	}

	@Override
	public List<CourseRegistration> getAllCourses(int userId) {

		return courseRegistrationRepository.getAllCourses(userId);
	}

	@Override
	public Course getCourseById(int id) {
		return courseRepository.findCourseByCourseId(id);
	}

	@Override
	public void deleteCourse(Course course) {
		courseRepository.delete(course);
	}

	@Override
	public void deleteRegisteredCourse(CourseRegistration courseRegistration) {
		courseRegistrationRepository.delete(courseRegistration);

	}

	@Override
	public CourseRegistration getRegisteredCourse(Course course) {
		return courseRegistrationRepository.findCourseRegistrationByCourse(course);
	}

	@Override
	public CourseRegistration assignCourse(CourseRegistration courseRegistration) {
		System.out.println(courseRegistration);
		courseRegistration.setStatus("Accepted");
		courseRegistration.setCompletionStatus("Not Completed");
		Date date = new Date();
		courseRegistration.setAssignedDate(date);
		courseRegistration.setRemarks("Accepted by Admin");
		courseRegistrationRepository.save(courseRegistration);
		return courseRegistration;
	}

	@Override
	public List<CourseRegistration> getAllEnrolledCourses() {
		return courseRegistrationRepository.getAllEnrolledCourses();
	}

	@Override
	public List<CourseRegistration> getAllAssignedCourses() {
		return courseRegistrationRepository.getAllAssignedCourses();
	}

	@Override
	public CourseRegistration getRegisteredCourseById(int id) {
		return courseRegistrationRepository.findCourseRegistrationById(id);
	}

	@Override
	public CourseRegistration assignCourseToUser(String courseName, String userName, String remarks) {
		Course course = courseRepository.findCourseByCourseName(courseName);
		User user = userRepository.findUserByFirstName(userName);
		System.out.println(course);
		CourseRegistration user_course = courseRegistrationRepository.findCourseRegistrationByCourseAndUser(course,
				user);
		if (user_course.getStatus().equalsIgnoreCase("Enrolled")) {
			user_course.setAssignedDate(new Date());
			user_course.setStatus("Accepted");
			user_course.setRemarks(remarks);
			user_course.setCompletionStatus("Not Completed");
			courseRegistrationRepository.save(user_course);
			return user_course;
		}

		return null;
	}

	@Override
	public CourseRegistration getRegisteredCourseByUserAndCourse(User user, Course course) {
		return courseRegistrationRepository.findCourseRegistrationByCourseAndUser(course, user);
	}

	@Override
	public List<CourseRegistration> getAllEnrolledCoursesOfUser(int id) {
		return courseRegistrationRepository.getAllEnrolledCoursesOfUser(id);
	}

	@Override
	public CourseRegistration changeSatus(CourseRegistration course) {
		return courseRegistrationRepository.save(course);
	}
}
