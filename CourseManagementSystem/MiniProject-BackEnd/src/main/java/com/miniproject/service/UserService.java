package com.miniproject.service;

import com.miniproject.model.User;

public interface UserService {

	public User registerUser(User user);

	public User getUserByEmailId(String emailId);

	public User getUserByEmailIdAndPassword(String emailId, String password);

}
