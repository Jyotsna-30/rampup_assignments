package com.miniproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.miniproject.model.Course;
import com.miniproject.model.CourseRegistration;
import com.miniproject.model.User;

public interface CourseRegistrationRepository extends JpaRepository<CourseRegistration, Integer> {

	@Query(value = "SELECT * FROM Course_Registration  where user_id = ?1", nativeQuery = true)
	List<CourseRegistration> getAllCourses(int userId);

	@Query(value = "SELECT * FROM Course_Registration  where status = 'Enrolled' ", nativeQuery = true)
	List<CourseRegistration> getAllEnrolledCourses();

	@Query(value = "SELECT * FROM Course_Registration  where status = 'Enrolled' AND user_id=?1 ", nativeQuery = true)
	List<CourseRegistration> getAllEnrolledCoursesOfUser(int id);

	@Query(value = "SELECT * FROM Course_Registration  where status = 'Accepted' ", nativeQuery = true)
	List<CourseRegistration> getAllAssignedCourses();

	CourseRegistration findCourseRegistrationByCourse(Course course);

	CourseRegistration findCourseRegistrationById(int id);

	CourseRegistration findCourseRegistrationByCourseAndUser(Course course, User user);
}
