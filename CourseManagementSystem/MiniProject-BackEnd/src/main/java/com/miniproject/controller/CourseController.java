package com.miniproject.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject.model.Course;
import com.miniproject.model.CourseRegistration;
import com.miniproject.service.CourseService;

@RestController
public class CourseController {
	@Autowired
	CourseService courseService;

	@GetMapping("/allCourses")
	public ResponseEntity<List<Course>> getAllCourses() throws Exception {
		List<Course> courses = courseService.getAllCourses();
		System.out.println("into controller");
		if (courses != null)
			return new ResponseEntity<>(courses, HttpStatus.OK);
		return new ResponseEntity<List<Course>>(HttpStatus.BAD_REQUEST);
	}

	@GetMapping("/getAllCourses/{id}")
	public ResponseEntity<List<Course>> getAllCoursesbyId(@PathVariable("id") int id) throws Exception {
		List<Course> courses = courseService.getAllCourses();
		List<CourseRegistration> enrolledCourses = courseService.getAllEnrolledCoursesOfUser(id);
		List<Course> newlist = new ArrayList<Course>();
		for (CourseRegistration courseRegistration : enrolledCourses) {
			Course course = courseService.getCourseById(courseRegistration.getCourse().getCourseId());
			newlist.add(course);
		}

		courses.removeAll(newlist);

		return new ResponseEntity<List<Course>>(courses, HttpStatus.OK);
	}

	@GetMapping("/course/{id}")
	public ResponseEntity<Course> getCourseById(@PathVariable("id") int id) {
		return new ResponseEntity<Course>(courseService.getCourseById(id), HttpStatus.OK);
	}

}
