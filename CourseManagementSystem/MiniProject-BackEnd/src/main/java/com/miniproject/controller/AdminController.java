package com.miniproject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject.model.Course;
import com.miniproject.model.CourseRegistration;
import com.miniproject.service.CourseService;

@RestController
public class AdminController {
	@Autowired
	CourseService courseService;

	@PostMapping("/admin/saveCourse")
	public ResponseEntity<Course> saveCourse(@RequestBody Course course) throws Exception {
		String tempCourseName = course.getCourseName();
		if (tempCourseName != null) {
			Course courseobject = courseService.getCourseByCourseName(tempCourseName);
			if (courseobject != null) {
				throw new Exception("Course with the same name already Exists");
			}
		}
		Course newCourse = courseService.saveCourse(course);
		return new ResponseEntity<>(newCourse, HttpStatus.CREATED);
	}

	@PutMapping("/admin/editCourse")
	public ResponseEntity<Course> editCourse(@RequestBody Course course) throws Exception {
		System.out.println(course);
		Course newCourse = courseService.saveCourse(course);
		return new ResponseEntity<>(newCourse, HttpStatus.CREATED);
	}

	@DeleteMapping("/admin/deleteCourse/{id}")
	public void deleteCourse(@PathVariable("id") int id) {
		Course course = courseService.getCourseById(id);
		System.out.println(course);
		CourseRegistration courseRegistration = courseService.getRegisteredCourse(course);
		if (courseRegistration != null)
			courseService.deleteRegisteredCourse(courseRegistration);

		courseService.deleteCourse(courseService.getCourseById(id));
	}

	@GetMapping("/admin/getEnrolledCourses")
	public ResponseEntity<List<CourseRegistration>> getEnrolledCourses() throws Exception {
		try {
			List<CourseRegistration> list = courseService.getAllEnrolledCourses();
			return new ResponseEntity<List<CourseRegistration>>(list, HttpStatus.OK);
		} catch (Exception e) {
			throw new Exception("No courses are Enrolled");
		}
	}

	@PutMapping("/admin/assignCourse")
	public ResponseEntity<CourseRegistration> assignCourse(@RequestBody CourseRegistration courseRegistration) {
		System.out.println("into controller");
		System.out.println(courseRegistration);
		CourseRegistration _courseRegistration = courseService.assignCourse(courseRegistration);
		return new ResponseEntity<CourseRegistration>(_courseRegistration, HttpStatus.OK);
	}

	@GetMapping("/admin/getAssignedCourses")
	public ResponseEntity<List<CourseRegistration>> getAllAssignedCourses() throws Exception {
		try {
			List<CourseRegistration> list = courseService.getAllAssignedCourses();
			return new ResponseEntity<List<CourseRegistration>>(list, HttpStatus.OK);
		} catch (Exception e) {
			throw new Exception("No courses are Enrolled");
		}
	}

	@DeleteMapping("/admin/deleteAssignedCourse/{id}")
	public void deleteAssignedCourse(@PathVariable("id") int id) {
		CourseRegistration assignedCourse = courseService.getRegisteredCourseById(id);
		courseService.deleteRegisteredCourse(assignedCourse);
	}

	@GetMapping("/admin/assignCourseuser")
	public ResponseEntity<CourseRegistration> assignCoursetoUser(
			@RequestParam(name = "courseName", required = true) String courseName,
			@RequestParam(name = "userName", required = true) String userName,
			@RequestParam(name = "remarks", required = false) String remarks) throws Exception {
		CourseRegistration courseRegistration = courseService.assignCourseToUser(courseName, userName, remarks);
		System.out.println("controller");
		System.out.println(courseRegistration);
		if (courseRegistration == null) {
			throw new Exception("Incorrect CourseName / UserName");
		}
		return new ResponseEntity<CourseRegistration>(courseRegistration, HttpStatus.OK);
	}

	@PostMapping("/admin/assignCourseuser")
	public ResponseEntity<CourseRegistration> _assignCoursetoUser(
			@RequestParam(name = "courseName", required = true) String courseName,
			@RequestParam(name = "userName", required = true) String userName,
			@RequestParam(name = "remarks", required = false) String remarks) throws Exception {
		CourseRegistration courseRegistration = courseService.assignCourseToUser(courseName, userName, remarks);
		System.out.println("controller");
		System.out.println(courseRegistration);
		if (courseRegistration == null) {
			throw new Exception("Incorrect CourseName / UserName");
		}
		return new ResponseEntity<CourseRegistration>(courseRegistration, HttpStatus.OK);
	}
}
