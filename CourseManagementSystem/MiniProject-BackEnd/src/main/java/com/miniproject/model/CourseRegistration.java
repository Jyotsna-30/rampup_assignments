package com.miniproject.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table
public class CourseRegistration {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@ManyToOne
	@JoinColumn(name = "userId")
	private User user;
	@ManyToOne
	@JoinColumn(name = "courseId")
	private Course course;

	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date assignedDate;
	private String status;
	private String remarks;
	private String completionStatus;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Date getAssignedDate() {
		return assignedDate;
	}

	public void setAssignedDate(Date assignedDate) {
		this.assignedDate = assignedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public CourseRegistration(User user, Course course) {
		super();

		this.user = user;
		this.course = course;
	}

	public CourseRegistration(User user, Course course, Date assignedDate, String status, String remarks,
			String completionStatus) {
		super();
		this.user = user;
		this.course = course;
		this.assignedDate = assignedDate;
		this.status = status;
		this.remarks = remarks;
		this.completionStatus = completionStatus;
	}

	public CourseRegistration() {
		super();
	}

	public String getCompletionStatus() {
		return completionStatus;
	}

	public void setCompletionStatus(String completionStatus) {
		this.completionStatus = completionStatus;
	}

	@Override
	public String toString() {
		return "CourseRegistration [id=" + id + ", user=" + user + ", course=" + course + ", assignedDate="
				+ assignedDate + ", status=" + status + ", completionStatus=" + completionStatus + "]";
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}
