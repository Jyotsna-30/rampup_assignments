import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';

import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { NgxPaginationModule } from 'ngx-pagination';

import { UserRegistrationComponent } from './user/user-registration/user-registration.component';
import { UserLoginComponent } from './user/user-login/user-login.component';
import { UserComponent } from './user/user/user.component';

import { MyCoursesComponent } from './user/my-courses/my-courses.component';
import { AdminComponent } from './admin/admin/admin.component';
import { SaveCourseComponent } from './admin/save-course/save-course.component';
import { EditCourseComponent } from './admin/edit-course/edit-course.component';
import { AllCoursesComponent } from './user/all-courses/all-courses.component';
import { CoursesComponent } from './admin/courses/courses.component';
import { CourseRequestsComponent } from './admin/course-requests/course-requests.component';
import { CoursesInfoComponent } from './admin/courses-info/courses-info.component';
import { SearchfilterPipe } from './searchfilter.pipe';
import { SearchCoursePipe } from './search-course.pipe';
import { AuthGuard } from './auth.guard';
@NgModule({
  declarations: [
    AppComponent,
    UserRegistrationComponent,
    UserLoginComponent,
    UserComponent,
    AdminComponent,
    SaveCourseComponent,
    EditCourseComponent,
    AllCoursesComponent,
    MyCoursesComponent,
    CoursesComponent,
    CourseRequestsComponent,
    CoursesInfoComponent,
    SearchfilterPipe,
    SearchCoursePipe,
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    Ng2SearchPipeModule,
    FormsModule,
    NgxPaginationModule,
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent],
})
export class AppModule {}
