import { AdminService } from 'src/app/services/admin.service';
import { CourseRegistration } from './../../classes/course-registration';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-courses-info',
  templateUrl: './courses-info.component.html',
  styleUrls: ['./courses-info.component.css'],
})
export class CoursesInfoComponent implements OnInit {
  message: string;
  assignedCourses: CourseRegistration[];
  searchValue: string;
  page: number = 1;
  empty: boolean;
  totalLength: number;
  constructor(private adminService: AdminService, private router: Router) {}
  AssignCourseForm = new FormGroup({
    courseName: new FormControl('', [Validators.required]),
    firstName: new FormControl('', [Validators.required]),
    remarks: new FormControl(''),
  });
  ngOnInit(): void {
    this.adminService.getAllAssignedCourses().subscribe(
      (data) => {
        this.assignedCourses = data;
        this.totalLength = this.assignedCourses.length;
        if (this.totalLength == 0) {
          this.empty = true;
        }
      },
      (error) => {
        console.log('error in fetching assigned courses');
      }
    );
  }

  deleteAssignedCourse(course: CourseRegistration) {
    this.adminService.deleteAssignedCourse(course.id).subscribe(
      (data) => {
        console.log('deleted the record');
        this.ngOnInit();
      },
      (error) => {
        console.log('failed in deleting the record');
      }
    );
  }

  assignCourse() {
    console.log(this.AssignCourseForm.controls['courseName'].value);
    this.adminService
      .assignCoursetoUser(
        this.AssignCourseForm.controls['courseName'].value,
        this.AssignCourseForm.controls['firstName'].value,
        this.AssignCourseForm.controls['remarks'].value
      )
      .subscribe(
        (data) => {
          this.message = 'Assigned Successfully';
          this.ngOnInit();
        },
        (error) => {
          this.message = 'Incorrect UserName / CourseName';
          console.log('Error in assign the course');
        }
      );
  }
}
