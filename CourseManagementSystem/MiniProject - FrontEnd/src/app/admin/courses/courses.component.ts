import { AdminService } from 'src/app/services/admin.service';
import { CourseService } from 'src/app/services/course.service';
import { Component, OnInit } from '@angular/core';
import { Course } from 'src/app/classes/course';
import { Router } from '@angular/router';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css'],
})
export class CoursesComponent implements OnInit {
  courses: Course[];
  course: Course;
  searchValue: string;
  page: number = 1;

  constructor(
    private courseService: CourseService,
    private router: Router,
    private adminService: AdminService
  ) {}

  ngOnInit(): void {
    this.courseService._getAllCourses().subscribe(
      (data) => {
        this.courses = data;
        console.log(this.courses);
      },
      (error) => {
        console.log('Error in getting courses');
      }
    );
  }

  editCourse(course: Course) {
    console.log(course.courseId);
    this.router.navigate(['/editcourse', course.courseId]);
  }

  deleteCourse(course: Course) {
    this.adminService.deleteCourse(course.courseId).subscribe(
      (data) => {
        console.log('deleted Successfully');
        this.ngOnInit();
      },
      (error) => {
        console.log(course);
        console.log('Failed to delete');
      }
    );
  }
}
