import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AdminService } from 'src/app/services/admin.service';
import { Course } from 'src/app/classes/course';
import { Router } from '@angular/router';

@Component({
  selector: 'app-save-course',
  templateUrl: './save-course.component.html',
  styleUrls: ['./save-course.component.css'],
})
export class SaveCourseComponent implements OnInit {
  course = new Course();
  constructor(private adminService: AdminService, private router: Router) {}
  saveCourseForm = new FormGroup({
    courseName: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
    link: new FormControl('', [Validators.required]),
  });

  ngOnInit(): void {}

  saveCourse() {
    this.course = this.saveCourseForm.value;
    this.adminService.saveCourse(this.course).subscribe(
      (data) => {
        this.router.navigate(['/admin']);
        console.log('Course saved successfully');
      },
      (error) => {
        console.log('Failed in saving the course');
      }
    );
  }
}
