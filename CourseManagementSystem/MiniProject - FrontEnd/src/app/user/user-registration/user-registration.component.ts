import { Component, OnInit, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';

import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/classes/user';

@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.css'],
})
export class UserRegistrationComponent implements OnInit {
  status: boolean;
  message: string;
  user: User = new User();
  constructor(private userService: UserService, private router: Router) {}
  userRegistrationForm = new FormGroup({
    firstName: new FormControl('', [Validators.required]),
    lastName: new FormControl('', [Validators.required]),
    phoneNumber: new FormControl('', [
      Validators.required,
      Validators.pattern(/^-?(0|[1-9]\d*)?$/),
      Validators.minLength(10),
      Validators.maxLength(10),
    ]),
    emailId: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required]),
    confirmPassword: new FormControl('', [Validators.required]),
  });

  ngOnInit(): void {}

  registerUser() {
    this.user = this.userRegistrationForm.value;
    const password = this.userRegistrationForm.controls['password'].value;
    const confirm_password = this.userRegistrationForm.controls[
      'confirmPassword'
    ].value;

    if (password == confirm_password) {
      this.userService.registerUser(this.user).subscribe(
        (data) => {
          console.log('user Registered Successfully');
          this.router.navigate(['/userlogin']);
          alert('Registered Successfully');
        },
        (error) => {
          this.status = true;
          console.log('This email Id is already registered');
          alert('This email Id is already registered');
        }
      );
    } else {
      console.log('Mismatch of password');
      this.message = 'Password and Confirm Password must be same';
    }
  }
}
