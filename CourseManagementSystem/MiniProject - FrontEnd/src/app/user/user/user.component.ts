import { UserService } from 'src/app/services/user.service';
import { CourseService } from './../../services/course.service';
import { Component, OnInit } from '@angular/core';
import { Course } from 'src/app/classes/course';
import { User } from 'src/app/classes/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit {
  constructor(
    private courseService: CourseService,
    private userService: UserService,
    private router: Router
  ) {}
  courses: Course[];
  user: User;
  id: string;
  ngOnInit(): void {
    this.id = localStorage.getItem('userId');
    this.userService.getUser(this.id).subscribe(
      (data) => {
        this.user = data;
        this.courseService.getAllCourses(this.user.userId).subscribe(
          (data) => {
            this.courses = data;
            console.log(this.courses);
          },
          (error) => {
            console.log('Error in getting courses');
          }
        );
        console.log(data);
      },
      (error) => console.log('error 1')
    );
  }

  logOut() {
    localStorage.clear();
    this.router.navigate['/userlogin'];
  }
}
