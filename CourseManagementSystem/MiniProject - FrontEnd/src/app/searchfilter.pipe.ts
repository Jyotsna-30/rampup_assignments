import { Pipe, PipeTransform } from '@angular/core';
import { CourseRegistration } from './classes/course-registration';

@Pipe({
  name: 'searchfilter',
})
export class SearchfilterPipe implements PipeTransform {
  transform(
    courses: CourseRegistration[],
    searchValue: string
  ): CourseRegistration[] {
    if (!courses || !searchValue) {
      return courses;
    }
    return courses.filter(
      (course) =>
        course.user.firstName
          .toLocaleLowerCase()
          .includes(searchValue.toLocaleLowerCase()) ||
        course.course.courseName
          .toLocaleLowerCase()
          .includes(searchValue.toLocaleLowerCase())
    );
  }
}
