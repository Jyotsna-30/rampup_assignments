import { CourseRegistration } from './../classes/course-registration';
import { Course } from './../classes/course';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../classes/user';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private baseUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = 'http://localhost:8080/user';
  }
  public registerUser(user: User): Observable<User> {
    return this.http.post<User>(`${this.baseUrl}/register`, user);
  }
  public userLogin(user: User): Observable<User> {
    return this.http.post<User>(`${this.baseUrl}/login`, user);
  }

  public enrollCourse(
    user: User,
    courseName: string
  ): Observable<CourseRegistration> {
    let params = new HttpParams().set('courseName', courseName);
    return this.http.post<CourseRegistration>(
      `${this.baseUrl}/enrollCourse`,
      user,
      {
        params: params,
      }
    );
  }

  public getUser(emailId: string): Observable<User> {
    let params = new HttpParams().set('emailId', emailId);
    return this.http.get<User>(`${this.baseUrl}/userinfo`, { params: params });
  }

  public getAllCourses(emailId: string): Observable<CourseRegistration[]> {
    return this.http.post<CourseRegistration[]>(
      `${this.baseUrl}/getAllCourses`,
      emailId
    );
  }

  public changeStatus(CourseReg: CourseRegistration) {
    return this.http.put<CourseRegistration>(
      `${this.baseUrl}/changeStatus`,
      CourseReg
    );
  }
}
