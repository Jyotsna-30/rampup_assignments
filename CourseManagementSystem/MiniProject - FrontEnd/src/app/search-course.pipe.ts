import { Pipe, PipeTransform } from '@angular/core';
import { Course } from './classes/course';

@Pipe({
  name: 'searchCourse',
})
export class SearchCoursePipe implements PipeTransform {
  transform(courses: Course[], searchValue: string): Course[] {
    if (!courses || !searchValue) {
      return courses;
    }
    return courses.filter(
      (course) =>
        course.courseName
          .toLocaleLowerCase()
          .includes(searchValue.toLocaleLowerCase()) ||
        course.description
          .toLocaleLowerCase()
          .includes(searchValue.toLocaleLowerCase())
    );
  }
}
